const withTM = require('next-transpile-modules')([
  '@goolie/frontend-components',
]);

module.exports = withTM({
  reactStrictMode: true,
});
