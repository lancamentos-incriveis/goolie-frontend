import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import external from 'rollup-plugin-peer-deps-external';
import renameNodeModules from 'rollup-plugin-rename-node-modules';

const rollup = () => {
  const input = 'src/index.ts';

  return [
    {
      input,
      output: {
        dir: 'dist/cjs',
        format: 'cjs',
        preserveModules: true,
        preserveModulesRoot: 'src',
        sourcemap: true,
      },
      plugins: [
        external(),
        nodeResolve(),
        commonjs(),
        typescript(),
        renameNodeModules('external'),
      ],
    },
    {
      input,
      output: {
        dir: 'dist/esm',
        format: 'esm',
        preserveModules: true,
        preserveModulesRoot: 'src',
        sourcemap: true,
      },
      plugins: [
        external(),
        nodeResolve(),
        typescript({ declaration: true, declarationDir: 'dist/esm/types' }),
        renameNodeModules('external'),
      ],
    },
  ];
};

export default rollup;
