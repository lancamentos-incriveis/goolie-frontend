import React, { useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import styled from 'styled-components';
import { REElement, TableData, ModalActionTypeEnum } from '../types';
import { REForm, Table } from '../';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

const FloatingAdd = styled(Fab)`
  position: absolute;
  bottom: 20px;
  right: 20px;
`;

const PageBox = styled(Box)`
  position: relative;
`;

interface PageProps {
  tableData: TableData;
  onSaveHandler(data: REElement): void;
  onDeleteHandler(data: REElement): void;
}

const Page = ({ tableData, onSaveHandler, onDeleteHandler }: PageProps) => {
  const [openAddModal, setAddModalOpen] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [activeElement, setActiveElement] = useState<REElement | undefined>(
    undefined
  );
  const [activeAction, setActiveAction] = useState<ModalActionTypeEnum | null>(
    null
  );

  const handleModalOpen = (type: ModalActionTypeEnum, el?: REElement) => {
    setActiveElement(el);
    setActiveAction(type);
    setAddModalOpen(true);
  };

  const handleModalClose = () => {
    setAddModalOpen(false);
  };

  const handleDeleteConfirm = (el: REElement) => {
    setActiveElement(el);
    setOpenDeleteDialog(true);
  };

  const handleDeleteDialogClose = (confirm: boolean) => {
    if (confirm && activeElement) onDeleteHandler(activeElement);
    setOpenDeleteDialog(false);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        <PageBox sx={{ height: '95vh' }}>
          <FloatingAdd
            color="primary"
            aria-label="add"
            onClick={() => handleModalOpen(ModalActionTypeEnum.add)}
          >
            <AddIcon />
          </FloatingAdd>
          <Table
            {...tableData}
            onViewHandler={(el) =>
              handleModalOpen(ModalActionTypeEnum.view, el)
            }
            onEditHandler={(el) =>
              handleModalOpen(ModalActionTypeEnum.edit, el)
            }
            onDeleteHandler={(el) => handleDeleteConfirm(el)}
          />
        </PageBox>
      </Container>
      <Dialog fullWidth={true} open={openAddModal} onClose={handleModalClose}>
        <DialogContent>
          <REForm
            onSubmitHandler={onSaveHandler}
            onCancelHandler={handleModalClose}
            item={activeElement}
            readOnly={activeAction === ModalActionTypeEnum.view}
          />
        </DialogContent>
      </Dialog>
      <Dialog open={openDeleteDialog} onClose={handleDeleteDialogClose}>
        <DialogTitle id="alert-dialog-title">
          {`Are You want to delete real estate with title: ${activeElement?.title}`}
        </DialogTitle>
        <DialogActions>
          <Button onClick={() => handleDeleteDialogClose(false)}>No</Button>
          <Button onClick={() => handleDeleteDialogClose(true)} autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default Page;
