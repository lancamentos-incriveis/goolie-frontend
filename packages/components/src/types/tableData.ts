import { REElement } from './reElement';

export interface TableData {
  headTitles: string[];
  elements: REElement[];
}
