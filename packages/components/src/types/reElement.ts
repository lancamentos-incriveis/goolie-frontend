export interface REElement {
  id: number;
  title: string;
  description: string;
  address: string;
  bathrooms: number;
  bedrooms: number;
  imageCount: number;
  price: string;
}
