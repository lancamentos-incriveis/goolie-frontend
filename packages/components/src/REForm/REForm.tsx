import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { REElement } from '../types';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import NumberFormat from 'react-number-format';

interface REFormProps {
  onSubmitHandler(el: REElement): void;
  onCancelHandler(): void;
  item?: REElement;
  readOnly: boolean;
}

const NumberFormatCustom = ({ inputRef, onChange, ...other }) => (
  <NumberFormat
    {...other}
    getInputRef={inputRef}
    onValueChange={(values) => {
      onChange({
        target: {
          name: props.name,
          value: values.value,
        },
      });
    }}
    thousandSeparator
    isNumericString
    allowNegative={false}
    prefix="R$ "
  />
);

const InputTextForm = ({
  type,
  errorMessage,
  registerFn,
  errors,
  fieldAttrs = {},
}: {
  type: string;
  errorMessage: string;
  registerFn: func;
  errors: any;
  fieldAttrs?: object;
}) => {
  const { inputRef, ...inputProps } = {
    ...registerFn(type, { required: errorMessage }),
  };

  return (
    <TextField
      name={type}
      label={type}
      variant="standard"
      required
      inputRef={inputRef}
      error={!!errors[type]}
      helperText={errors?.[type]?.message}
      {...fieldAttrs}
      {...inputProps}
    />
  );
};

const REForm = ({
  onSubmitHandler,
  onCancelHandler,
  item,
  readOnly = false,
}: REFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<REElement>({
    shouldFocusError: true,
    criteriaMode: 'all',
  });

  const onSubmit = (data) => {
    onSubmitHandler(data as REElement);
  };

  return (
    <Container maxWidth="md">
      <Box
        sx={{ flexGrow: 1 }}
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        noValidate
        autoComplete="off"
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <InputTextForm
              type="title"
              errorMessage="Please enter a real estate title"
              errors={errors}
              registerFn={register}
              fieldAttrs={{
                fullWidth: true,
                defaultValue: item?.title,
                InputProps: {
                  readOnly,
                },
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <InputTextForm
              type="description"
              errorMessage="Please enter real estate description"
              errors={errors}
              registerFn={register}
              fieldAttrs={{
                multiline: true,
                minRows: 5,
                fullWidth: true,
                defaultValue: item?.description,
                InputProps: {
                  readOnly,
                },
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <InputTextForm
              type="address"
              errorMessage="Please enter real estate full address"
              errors={errors}
              registerFn={register}
              fieldAttrs={{
                fullWidth: true,
                defaultValue: item?.address,
                InputProps: {
                  readOnly,
                },
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <Grid container spacing={2}>
              <Grid item xs>
                <InputTextForm
                  type="bedrooms"
                  errorMessage="Please enter number of bedrooms"
                  errors={errors}
                  registerFn={register}
                  fieldAttrs={{
                    type: 'number',
                    fullWidth: true,
                    InputLabelProps: {
                      shrink: true,
                    },
                    InputProps: {
                      inputProps: {
                        max: 10,
                        min: 0,
                      },
                      readOnly,
                    },
                    defaultValue: item?.bedrooms,
                  }}
                />
              </Grid>
              <Grid item xs>
                <InputTextForm
                  type="bathrooms"
                  errorMessage="Please enter number of bathrooms"
                  errors={errors}
                  registerFn={register}
                  fieldAttrs={{
                    type: 'number',
                    fullWidth: true,
                    InputLabelProps: {
                      shrink: true,
                    },
                    InputProps: {
                      inputProps: {
                        max: 10,
                        min: 0,
                      },
                      readOnly,
                    },
                    defaultValue: item?.bathrooms,
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={8} align="right">
            <InputTextForm
              type="price"
              errorMessage="Please enter number of bathrooms"
              errors={errors}
              registerFn={register}
              fieldAttrs={{
                InputLabelProps: {
                  shrink: true,
                },
                InputProps: {
                  readOnly,
                  inputComponent: NumberFormatCustom,
                },
                defaultValue: item?.price,
              }}
            />
          </Grid>

          <Grid container xs={12} marginTop={4}>
            {!readOnly && (
              <>
                <Grid item align="right">
                  <Button type="submit" variant="contained">
                    Save
                  </Button>
                </Grid>
                <Grid item align="right">
                  <Button variant="text" onClick={onCancelHandler}>
                    Cancel
                  </Button>
                </Grid>
              </>
            )}
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};
export default REForm;
