import * as React from 'react';
import styled from 'styled-components';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { REElement } from '../types';

interface RETableProps {
  headTitles: string[];
  elements: REElement[];
  onViewHandler(el: REElement): void;
  onEditHandler(el: REElement): void;
  onDeleteHandler(el: REElement): void;
}

const StyledHeadRow = styled(TableRow)`
  background-color: blue;
`;

const StyledHeadCell = styled(TableCell)`
  color: white;
  font-weight: 600;
`;

const ClickableCell = styled(TableCell)`
  text-align: center;
  padding: 8px;
  :hover {
    background-color: grey;
  }
  cursor: pointer;
`;

const RETable = ({
  elements,
  headTitles,
  onViewHandler,
  onEditHandler,
  onDeleteHandler,
}: RETableProps) => {
  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%' }}>
        <TableContainer>
          <Table
            sx={{ minWidth: 500 }}
            aria-labelledby="tableTitle"
            size={'medium'}
          >
            <TableHead>
              <StyledHeadRow>
                {headTitles.map((title) => (
                  <StyledHeadCell>{title}</StyledHeadCell>
                ))}
                <TableCell colSpan={3} />
              </StyledHeadRow>
            </TableHead>
            <TableBody>
              {elements.map((element) => {
                return (
                  <TableRow>
                    <TableCell>{element.title}</TableCell>
                    <TableCell>{element.description}</TableCell>
                    <TableCell>{element.address}</TableCell>
                    <TableCell>{element.bedrooms}</TableCell>
                    <TableCell>{element.bathrooms}</TableCell>
                    <TableCell>R$ {element.price}</TableCell>
                    <ClickableCell onClick={() => onViewHandler(element)}>
                      <VisibilityIcon />
                    </ClickableCell>
                    <ClickableCell onClick={() => onEditHandler(element)}>
                      <EditIcon />
                    </ClickableCell>
                    <ClickableCell onClick={() => onDeleteHandler(element)}>
                      <DeleteIcon />
                    </ClickableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Box>
  );
};

export default RETable;
