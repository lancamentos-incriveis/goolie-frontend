import React from 'react';

import { Page } from '@goolie/frontend-components';

export default {
  title: 'Page',
  component: Page,
  argTypes: {},
};

const tableData = {
  headTitles: [
    'título',
    'descrição',
    'endereço',
    'n quartos',
    'n banheiros',
    'preço',
  ],
  elements: [
    {
      id: 1,
      address: 'rua rua blablabalb 111 123',
      bathrooms: 3,
      bedrooms: 3,
      description: 'ótimo ap 1',
      imageCount: 1,
      price: '600.000',
      title: 'ótimo ap 1',
    },
    {
      id: 2,
      address: 'rua rua blablabalb 111 123',
      bathrooms: 2,
      bedrooms: 1,
      description: 'ótimo ap 2',
      imageCount: 1,
      price: '600.000',
      title: 'ótimo ap 2',
    },
    {
      id: 3,
      address: 'rua rua blablabalb 111 123',
      bathrooms: 2,
      bedrooms: 2,
      description: 'ótimo ap 3',
      imageCount: 3,
      price: '600.000',
      title: 'ótimo ap 3',
    },
  ],
};

const Template = (args) => <Page {...args} />;

export const Default = Template.bind({});
Default.args = {
  tableData,
};
