import React from 'react';

import { REForm } from '@goolie/frontend-components';

export default {
  title: 'REForm',
  component: REForm,
  argTypes: {
    onSubmitHandler: {
      action: 'submit handled',
    },
    onCancelHandler: {
      action: 'cancel handled',
    },
  },
};

const Template = (args) => <REForm {...args} />;

export const Default = Template.bind({});
Default.args = {};

const reFilled = {
  id: 3,
  address: 'rua rua blablabalb 111 123',
  bathrooms: 2,
  bedrooms: 2,
  description: 'ótimo ap 3',
  imageCount: 3,
  price: '600.000',
  title: 'ótimo ap 3',
};

export const Filled = Template.bind({});
Filled.args = {
  item: reFilled,
};

export const ReadOnly = Template.bind({});
ReadOnly.args = {
  item: reFilled,
  readOnly: true,
};
