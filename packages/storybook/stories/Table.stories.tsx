import React from 'react';

import { Table } from '@goolie/frontend-components';
import { REElement } from '@goolie/frontend-components/src/types';

export default {
  title: 'Table',
  component: Table,
  argTypes: {
    onViewHandler: {
      action: 'view handler',
    },
    onEditHandler: {
      action: 'edit handler',
    },
    onDeleteHandler: {
      action: 'delete handler',
    },
  },
};

const Template = (args) => <Table {...args} />;

const headTitles = [
  'título',
  'descrição',
  'endereço',
  'n quartos',
  'n banheiros',
  'preço',
];

const elements: REElement[] = [
  {
    id: 1,
    address: 'rua rua blablabalb 111 123',
    bathrooms: 3,
    bedrooms: 3,
    description: 'ótimo ap 1',
    imageCount: 1,
    price: '600.000',
    title: 'ótimo ap 1',
  },
  {
    id: 2,
    address: 'rua rua blablabalb 111 123',
    bathrooms: 2,
    bedrooms: 1,
    description: 'ótimo ap 2',
    imageCount: 1,
    price: '600.000',
    title: 'ótimo ap 2',
  },
  {
    id: 3,
    address: 'rua rua blablabalb 111 123',
    bathrooms: 2,
    bedrooms: 2,
    description: 'ótimo ap 3',
    imageCount: 3,
    price: '600.000',
    title: 'ótimo ap 3',
  },
];

export const Default = Template.bind({});

Default.args = {
  headTitles,
  elements,
};
